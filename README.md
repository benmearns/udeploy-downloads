# README #

### What is this repository for? ###

* Code for UD's software download site
* Includes tracking of downloads using LDAP from CAS

### What do I need to do to get this to work ###

* You need a "credentials.php" in the top directory with a valid credentials string for a postgres database in the form <?php $dsn="pgsql://USERNAME:PASSWORD@HOST/DBNAME"
* Of course that presupposes a postgres database server/database
* You need a "local.php" in the top directory, which sets variables that depend on location.  Right now the only location-dependent variable is $THIS_SERVICE, so local.php could be <?php $THIS_SERVICE = "downloads"; ?>
* You must have an equivalent approved CAS service.  For more info: http://www.udel.edu/itwebdev/cas.html