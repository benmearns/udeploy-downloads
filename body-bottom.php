<?php
/*
  *  content section
*/
 $html_out  = '';
if (isset ($_SESSION[$SERVICE_ID])){
     $html_out .=  '<p align="center"><a href="https://cas.nss.udel.edu/cas/logout?service=http://css-rdms1.win.udel.edu/downloads/cas_logout.php">Logout</a></p>' . "\n";
  }


/*
  *  footer section
  * http://blog.nazdrave.net/?p=626
  * Using functions/expressions in HEREDOC strings
*/
$time = Date('Y');
$footer =<<<EOHTML
<ul class="small" id="exitlinks">
<li><a href="//www.udel.edu">UD Home</a></li>
<li><span data-dot="middot"></span><a href="//www.it.udel.edu">IT Home</a></li>
<li><span data-dot="middot"></span><a href="//udeploy.udel.edu">UDeploy Home</a></li>
</ul>
</div>
</body>
</html>
EOHTML;

echo $footer;
/*
// full footer commented out below.

<footer id="ud_brand_footer" role="contentinfo" itemscope="" itemtype="http://schema.org/EducationalOrganization">
  <div id="footcont" class="udwrap row">
    <div id="footerdiv" class="nine columns">
      <ul class="footerinfo">
        <li>
          <span data-dot="firstdot" itemprop="name">Information Technologies</span>
          <span data-dot="middot" itemprop="name">University of Delaware</span>
            <address itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
              <span itemprop="addressLocality">  <span data-dot="middot"> Newark</span>, </span>
              <span itemprop="addressRegion">DE </span>
              <span itemprop="postalCode"> 19716</span>
              <span data-dot="middot" itemprop="addressCountry">USA</span>
            </address>
          </li>
          <li>Phone: <a href="tel:3028316000" itemprop="telephone">(302) 831-6000</a>
          <small id="date">© {$time}</small>
          </li>
        </ul>
       <!-- <ul id="udlinks" class="small">
          <li><span data-dot="firstdot"></span><a target="_blank" href="https://delaware.qualtrics.com/SE?SID=SV_5j7YfTePPlSOj3e&SVID=Prod">Comments</a></li>
          <li><span data-dot="middot"></span><a href="mailto: conslut@udel.edu?subject=UDeploy Contact">Contact Us</a></li>
          <li><span data-dot="middot"></span><a target="_blank" href="http://www.udel.edu/compliance">UD Compliance Hotline</a></li>
          <li><span data-dot="middot"></span><a target="_blank" href="http://www.udel.edu/aboutus/legalnotices.html">Legal Notices</a></li>
    </ul>-->
</div>
<div class="three columns" id="ftlogo" itemscope="" itemtype="http://schema.org/ImageObject">
<a href="http://www.udel.edu">
          <img width="136" height="59" src="img/UD-footer-blue.png" class="right hires" alt="University of Delaware">
</a>
</div>
</div>
</footer>
*/
?>