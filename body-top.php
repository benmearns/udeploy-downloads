<?php
/*
  *  <head> section
*/
$head =<<<EOHTML
<!doctype html>
<html class="no-js" lang="en">
<head>
    <title>University of Delaware: UDeploy Software Licensed Distribution Web Site</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
 <meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="copyright" content="2014, University of Delaware">
<meta name="author" content="University of Delaware: Client Support & Services">
<link rel="shortcut icon" href="img/favicon.ico">
<link media="all" type="text/css" href="//fonts.googleapis.com/css?family=Lato:400,300&subset=latin" id="et-gf-lato-css" rel="stylesheet">
<link media="screen" href="css/default.css" rel="stylesheet">
<link media="screen" href="css/ud-styles.css" rel="stylesheet">
</head>
<body>
EOHTML;

echo $head;

/*
  *  header section
*/
$header =<<<EOHTML
<header id="udbrand_header" role="banner" class="clearfix">
  <div class="udwrap clearfix" id="headcont">
      <h1 id="udbrand_logo">
        <a href="http://www.udel.edu/" title="University of Delaware"><img src="img/UD-header-blue.png" class="left hires"></a>
      </h1>
  </div>
</header>

<header id="main-header">
  <div class="container">
    <div class="row" id="top-area">
      <div class="eight columns">
        <header id="subheader">
          <a href="http://sites.udel.edu/udeploy/"><img id="logo" alt="UDeploy" src="img/logo-udeploy.png"></a>
        </header>
      </div>
      <div class="four columns desc">
        <h2>UDeploy is a software licensed distribution web site for use by University of Delaware students, faculty and staff.</h2>
      </div>
    </div>
  </div>
</header>
<div id="wrapper">
  <div class="row">
    <div class="twelve columns">
EOHTML;

echo $header;
?>