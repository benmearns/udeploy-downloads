<?php
if (isset($_GET['download'])){
	$download_id = $_GET['download'];
	$_SESSION['ud_download_id'] = $_GET['download'];
	if (isset ($_SERVER['HTTP_REFERER'])){
		$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
	} 
} elseif (isset($_SESSION['ud_download_id'])) {
	$download_id = $_SESSION['ud_download_id'];
} else {
	include('body-top.php');
	if (isset ($_SERVER['HTTP_REFERER'])){
		$referer = $_SERVER['HTTP_REFERER'];
	} else {
		$referer = 'No referer detected';
	}
	echo "We have not detected a software selection to download.  Please notify the page that refered you here ($referer) that their link is broken.";
	include('body-bottom.php');
	exit;
}
?>