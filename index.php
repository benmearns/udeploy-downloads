  <?php

include('credentials.php');
include('local.php');

// GLOBAL DATA ======================================================
$STATUS = "PRODUCTION";
$SERVICE_ID = sha1($THIS_SERVICE);

require_once 'DB.php';

$dbh =& DB::connect ($dsn);
if (PEAR::isError($dbh)) {
    die($dbh->getMessage());
}
$CAS_SERVER = (isset($_SESSION['CAS_SERVER']) ? $_SESSION['CAS_SERVER'] : null);
if ($CAS_SERVER){
$CAS_LOGOUT = $_SESSION['CAS_SERVER'] . 'logout';
} else {
$CAS_LOGOUT = "https://webqa.mis.udel.edu/cas/" . 'logout';
}
$sw_name = '';
$sw_license = FALSE;

require('auth-cas.php');

if (!isset ($_SESSION[$SERVICE_ID])){
  header("Location: http://css-rdms1.win.udel.edu/$THIS_SERVICE/login.php");
  exit;
} else {
  $query_sql = "SELECT name,url,class,license FROM downloads WHERE id = $download_id;";
      
  $data =& $dbh->getRow($query_sql, array(), DB_FETCHMODE_ASSOC);

  if (PEAR::isError($data)) {
    die($data->getMessage());
  } 

  $_SESSION['sw_name']=$data['name'];
  $_SESSION['sw_license']=$data['license'];
  $_SESSION['sw_url']=$data['url'];
  $_SESSION['sw_class']=$data['class'];

  if(isset($data['url'])){$sw_url = $data['url'];}
  if(isset($data['name'])){$sw_name = $data['name'];}
  if(isset($data['license'])){$sw_license = $data['license'];}
  
  include('body-top.php');
  $class = $data['class'];
  $classLength = strlen($class);
  $ldapClass = $_SESSION['cas_data']['PERSONTYPE'];
  
  //temporary fix, should update all database facultystaff to staff
  if ($class == 'facultystaff'){
	$class = 'staff';
  }
  
  if ($classLength > 0){

if ($class == 'either' || (stripos($ldapClass, $class) !== FALSE && $class == 'student') || (stripos($ldapClass, $class) !== FALSE && stripos($ldapClass, 'MISC') === FALSE)){
	if ($sw_url <> ''){
      print "<iframe style=\"display:none;\" src=\"http://css-rdms1.win.udel.edu/$THIS_SERVICE/getdownload.php?token=$ticket&id=$SERVICE_ID&download=$download_id\"></iframe>
      <center><h1>$sw_name</h1>Your download will start automatically in a few moments.<h3>Problems with the download? Use this <a href=\"http://css-rdms1.win.udel.edu/$THIS_SERVICE/getdownload.php?token=$ticket&id=$SERVICE_ID&download=$download_id\">direct link</a>.</h3><h4>Be sure to <a
href=\"https://cas.nss.udel.edu/cas/logout?service=http://css-rdms1.win.udel.edu/$THIS_SERVICE/cas_logout.php\">logout</a> after your download completes.</h4><p>If you are still experiencing problems, contact the <a href='mailto:consult@udel.edu'>Support Center</a>.</p></center></div></div>";
	}
	if ($sw_license <> ''){
    print "<div class=\"row\"><div class=\"eight columns center license_box\"><ul><li>This software has an associated license number (e.g., registration code, authorization code, file installation key, etc.).</li><li>You'll be prompted to enter this code when you are installing the software.</li><li>The code is: <span style=\"font-size:21px\">$sw_license</span></li></ul></div></div>";
	}
  } else {
    print "<div class=\"row\"><div class=\"eight columns center license_box\"><h3>You must be $class to download this software.</h3><p>If you are experiencing problems, contact the <a href='mailto:consult@udel.edu'>Support Center</a>.</p><p>Close this tab to return to the previous page.</p></div></div>";
    //error_log('failure ' . json_encode($_SESSION));
	$content = var_export ($_SESSION['cas_data'], TRUE);
	$file = dirname( __FILE__ ) . '\permissionDenied';
	file_put_contents($file, $content,FILE_APPEND);
  }
 } 
  else {
	if ($sw_url <> ''){
    print "<iframe style=\"display:none;\" src=\"http://css-rdms1.win.udel.edu/$THIS_SERVICE/getdownload.php?token=$ticket&id=$SERVICE_ID&download=$download_id\"></iframe>
      <center><h1>$sw_name</h1>Your download will start automatically in a few moments.<h3>Problems with the download? Use this <a href=\"http://css-rdms1.win.udel.edu/$THIS_SERVICE/getdownload.php?token=$ticket&id=$SERVICE_ID&download=$download_id\">direct link</a>.</h3><h4>Be sure to <a
href=\"https://cas.nss.udel.edu/cas/logout?service=http://css-rdms1.win.udel.edu/$THIS_SERVICE/cas_logout.php\">logout</a> after your download completes.</h4><p>If you are still experiencing problems, contact the <a href='mailto:consult@udel.edu'>Support Center</a>.</p></center></div></div>";
	}
	if ($sw_license <> ''){
    print "<div class=\"row\"><div class=\"eight columns center license_box\"><ul><li>This software has an associated license number (e.g., registration code, authorization code, file installation key, etc.).</li><li>You'll be prompted to enter this code when you are installing the software.</li><li>The code is: <span style=\"font-size:21px\">$sw_license</span></li></ul></div></div>";
	}
  }

 
  // *** this part will print the license code KEMPISTA ***
    // *** print $_SESSION[license];
    include('body-bottom.php');
}
?>